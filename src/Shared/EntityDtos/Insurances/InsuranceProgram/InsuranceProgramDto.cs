﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.EntityDtos.Insurances
{
    public class InsuranceProgramDto
    {
        public int ProgramId { get; set; }
        public string? Name { get; set; }
    }
}
