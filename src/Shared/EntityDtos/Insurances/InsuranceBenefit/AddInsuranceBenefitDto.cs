﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.EntityDtos.Insurances.InsuranceBenefit
{
    public class AddInsuranceBenefitDto
    {
        public string? BenefitName { get; set; }
        public string? Description { get; set; }
    }
}
