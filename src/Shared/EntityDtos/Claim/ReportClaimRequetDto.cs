﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.EntityDtos.Claim
{
    public class ReportClaimRequetDto
    {
        public int status {get; set;}
        public float? total { get; set; }
    }
}
