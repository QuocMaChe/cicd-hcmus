﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.EntityDtos.Contract
{
    public class RegisterContractHealthConditionDto
    {
        public int Id { get; set; }
        public bool Status { get; set; }
    }
}
