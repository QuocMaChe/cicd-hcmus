#!/bin/bash

WEBHOOK_URL="https://hooks.slack.com/services/your/webhook/url"

MESSAGE=":rotating_light: Test failed in pipeline!"

function notify_slack {
    curl -X POST -H 'Content-type: application/json' --data "{\"text\":\"$MESSAGE\"}" $WEBHOOK_URL
}

if [ "$CI_JOB_STATUS" == "failed" ]; then
    notify_slack
fi
